### Cordova App

`cordova create <folder_name> com.<package_name>.<project_name> <app_name>`

The project folder will be created with the structure

```
hooks/
platforms/
plugins/
www/
config.xml
package.json

```

To add plugin

```
cordova plugin add cordova-plugin-inappbrowser

```

To add platform

```
cordova platform add ios
cordova platform add browser
cordova platform add android

```

Build react project (production build) and move the files into `www/` inside the
cordova project

Run

```
cordova emulate android - To get the app running in the emulator or

cordova run android - To get it as an app in android phone

```

To get the app in phone

Connect the phone using USB and enable USB Debugging.

Debug inspect link - chrome://inspect

Compile your react app using the command npm run build. Use the env file
.env.app while building the app

Place your compiled HTML and JS files from React App into the www/ folder

Then run the cordova app using cordova run android
